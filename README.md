# Restaurant Console Application

The Restaurant Console Application is a Java-based console program that allows users to interact with a restaurant menu, place orders, and view their orders. The application demonstrates basic object-oriented programming concepts and is built using Maven for dependency management and JUnit for testing.

## Visible Features

- The application start menu
- Browse all menu items and their details
- Place an order for menu items
- View current order details
- Exit the application

## Order Management

When using the application, users can:

- View the restaurant menu
- Add items to their order
- View their current order

## Menu

The menu displays:

- Item ID
- Item name
- Item price

## Technologies

- **Java** - for the programming language
- **Maven** - for project management and dependency handling
- **JUnit** - for unit testing
- **Mockito** - for mock testing

## Getting Started

### Prerequisites

Ensure you have the following installed on your machine:

- [Java JDK](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) (version 8 or later)
- [Apache Maven](https://maven.apache.org/install.html) (version 3.6.0 or later)

## Project Structure
The project is organized into several packages and classes, each responsible for specific functionalities. Below is a detailed breakdown of the main components and their roles.

### Packages and Classes

#### 1. `restaurant`
- **Main.java**
  - Entry point of the application.
  - Initializes repositories, controller, and the engine.
  - Runs the engine to start the application.

#### 2. `restaurant.core`
- **ControllerImpl.java**
  - Implements the `Controller` interface.
  - Handles operations such as adding healthy foods, beverages, tables, reserving tables, and managing orders.
- **EngineImpl.java**
  - Implements the `Engine` interface.
  - Controls the application's main loop, processes user inputs, and executes commands.

#### 3. `restaurant.core.interfaces`
- **Controller.java**
  - Defines methods for managing the restaurant's operations.
- **Engine.java**
  - Defines the method to run the engine.

#### 4. `restaurant.entities.drinks`
- **BaseBeverage.java**
  - Represents a basic beverage with properties like name, counter, price, and brand.
- **Fresh.java**
  - Extends `BaseBeverage`.
  - Represents a fresh beverage.
- **Smoothie.java**
  - Extends `BaseBeverage`.
  - Represents a smoothie beverage.

#### 5. `restaurant.entities.drinks.interfaces`
- **Beverages.java**
  - Defines the properties and methods for beverages.

#### 6. `restaurant.entities.healthyFoods`
- **Food.java**
  - Represents a basic food item with properties like name, portion, and price.
- **Salad.java**
  - Extends `Food`.
  - Represents a salad.
- **VeganBiscuit.java**
  - Extends `Food`.
  - Represents a vegan biscuit.

#### 7. `restaurant.entities.healthyFoods.interfaces`
- **HealthyFood.java**
  - Defines the properties and methods for healthy foods.

#### 8. `restaurant.entities.tables`
- **BaseTable.java**
  - Represents a basic table with properties and methods for managing orders and reservations.
- **Indoors.java**
  - Extends `BaseTable`.
  - Represents an indoor table.
- **InGarden.java**
  - Extends `BaseTable`.
  - Represents a garden table.

#### 9. `restaurant.entities.tables.interfaces`
- **Table.java**
  - Defines the properties and methods for tables.

#### 10. `restaurant.io`
- **ConsoleReader.java**
  - Implements the `InputReader` interface.
  - Reads input from the console.
- **ConsoleWriter.java**
  - Implements the `OutputWriter` interface.
  - Writes output to the console.

#### 11. `restaurant.io.interfaces`
- **InputReader.java**
  - Defines the method for reading input.
- **OutputWriter.java**
  - Defines the method for writing output.

#### 12. `restaurant.repositories`
- **BeverageRepositoryImp.java**
  - Implements `BeverageRepository`.
  - Manages beverage entities.
- **HealthFoodRepositoryImp.java**
  - Implements `HealthFoodRepository`.
  - Manages healthy food entities.
- **TableRepositoryImp.java**
  - Implements `TableRepository`.
  - Manages table entities.

#### 13. `restaurant.repositories.interfaces`
- **BeverageRepository.java**
  - Defines methods for managing beverages.
- **HealthFoodRepository.java**
  - Defines methods for managing healthy foods.
- **Repository.java**
  - Defines general methods for repositories.
- **TableRepository.java**
  - Defines methods for managing tables.

#### 14. `restaurant.common`
- **ExceptionMessages.java**
  - Contains error messages used throughout the application.
- **OutputMessages.java**
  - Contains success messages used throughout the application.

#### 15. `restaurant.common.enums`
- **BeveragesType.java**
  - Enumerates types of beverages.
- **Commands.java**
  - Enumerates commands for the engine.
- **HealthyFoodType.java**
  - Enumerates types of healthy foods.
- **TableType.java**
  - Enumerates types of tables.


## Test Classes

The project includes several test classes to ensure the functionality and reliability of the different components. Below is a detailed breakdown of the test classes and their purposes.

### Test Packages and Classes

#### 1. `restaurantTest.core`
- **ControllerTests.java**
  - Contains unit tests for the `ControllerImpl` class.
  - Verifies the correctness of operations such as adding healthy foods, beverages, and tables, as well as reserving tables and managing orders.

#### 2. `restaurantTest.models`
- **Drinks_Test.java**
  - Contains unit tests for the `BaseBeverage`, `Fresh`, and `Smoothie` classes.
  - Ensures the correct behavior and properties of different beverage entities.
- **Foods_Test.java**
  - Contains unit tests for the `Food`, `Salad`, and `VeganBiscuit` classes.
  - Validates the functionality and properties of various healthy food entities.
- **Tables_Test.java**
  - Contains unit tests for the `BaseTable`, `Indoors`, and `InGarden` classes.
  - Tests the operations and properties of different table types.

#### 3. `restaurantTest.repositories`
- **BeverageRepository_Test.java**
  - Contains unit tests for the `BeverageRepositoryImp` class.
  - Ensures the correct management of beverage entities within the repository.
- **HealthFoodRepository_Test.java**
  - Contains unit tests for the `HealthFoodRepositoryImp` class.
  - Verifies the proper handling of healthy food entities in the repository.
- **TableRepository_Test.java**
  - Contains unit tests for the `TableRepositoryImp` class.
  - Tests the functionality and management of table entities in the repository.
