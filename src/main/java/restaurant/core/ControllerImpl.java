package restaurant.core;

import restaurant.common.ExceptionMessages;
import restaurant.common.OutputMessages;
import restaurant.common.enums.BeveragesType;
import restaurant.common.enums.HealthyFoodType;
import restaurant.common.enums.TableType;
import restaurant.core.interfaces.Controller;
import restaurant.entities.drinks.BaseBeverage;
import restaurant.entities.drinks.Fresh;
import restaurant.entities.drinks.Smoothie;
import restaurant.entities.healthyFoods.Food;
import restaurant.entities.healthyFoods.Salad;
import restaurant.entities.healthyFoods.VeganBiscuit;
import restaurant.entities.healthyFoods.interfaces.HealthyFood;
import restaurant.entities.drinks.interfaces.Beverages;
import restaurant.entities.tables.BaseTable;
import restaurant.entities.tables.InGarden;
import restaurant.entities.tables.Indoors;
import restaurant.entities.tables.interfaces.Table;
import restaurant.repositories.interfaces.*;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {
    private final HealthFoodRepository<HealthyFood> healthFoodRepository;
    private final BeverageRepository<Beverages> beverageRepository;
    private final TableRepository<Table> tableRepository;
    private double total;

    public ControllerImpl(HealthFoodRepository<HealthyFood> healthFoodRepository,
                          BeverageRepository<Beverages> beverageRepository,
                          TableRepository<Table> tableRepository) {
        this.healthFoodRepository = healthFoodRepository;
        this.beverageRepository = beverageRepository;
        this.tableRepository = tableRepository;
    }

    @Override
    public String addHealthyFood(String type, double price, String name) {
        Food food = new Food();
        if (type.equals(HealthyFoodType.Salad.toString())){
        food = new Salad(name, price);
        }if(type.equals(HealthyFoodType.VeganBiscuits.toString())){
            food = new VeganBiscuit(name, price);
        }if(healthFoodRepository.foodByName(name).getName() != null){
            throw new IllegalArgumentException(String.format(ExceptionMessages.FOOD_EXIST, name));
        }
            healthFoodRepository.add(food);

        return String.format(OutputMessages.FOOD_ADDED, name);
    }


    @Override
    public String addBeverage(String type, int counter, String brand, String name){
        Beverages beverage = null;
        if (type.equals(BeveragesType.Fresh.toString())){
            beverage = new Fresh(name, counter, brand);
        } if (type.equals(BeveragesType.Smoothie.toString())){
            beverage = new Smoothie(name, counter, brand);
        }if (beverageRepository.beverageByName(name, brand).getName() != null){
            throw new IllegalArgumentException(String.format(ExceptionMessages.BEVERAGE_EXIST, name));
        }
        beverageRepository.add(beverage);
        //TODO:
        return String.format(OutputMessages.BEVERAGE_ADDED, type , brand);
    }

    @Override
    public String addTable(String type, int tableNumber, int capacity) {
        Table table = new BaseTable();
        if (type.equals(TableType.Indoors.toString())){
            table = new Indoors(tableNumber, capacity);
        } if (type.equals(TableType.InGarden.toString())){
            table = new InGarden(tableNumber, capacity);
        } if (tableRepository.byNumber(tableNumber).getTableNumber() != 0){
            throw new IllegalArgumentException(String.format(ExceptionMessages.TABLE_IS_ALREADY_ADDED, tableNumber));
        }
        tableRepository.add(table);
        //TODO:
        return String.format(OutputMessages.TABLE_ADDED, tableNumber);
    }

    @Override
    public String reserve(int numberOfPeople) {
        Table table3 = new BaseTable(0 , 1, 0);
        if (numberOfPeople<1){
            throw new IllegalArgumentException(ExceptionMessages.INVALID_NUMBER_OF_PEOPLE);
        }
        for (Table table : tableRepository.getAllEntities()) {
            if (table.getSize() >= numberOfPeople && !table.isReservedTable()) {
                table3 = table;
                break;
            }
        }for (Table table : tableRepository.getAllEntities()) {
            if (table.getSize() >= numberOfPeople && table.getSize()<table3.getSize() && !table.isReservedTable()) {
                table3 = table;
            }
        }
        tableRepository.byNumber(table3.getTableNumber()).reserve(numberOfPeople);
        if (table3.getTableNumber() == 0) {
            return String.format(OutputMessages.RESERVATION_NOT_POSSIBLE, numberOfPeople);
        }//TODO:
        return String.format(OutputMessages.TABLE_RESERVED, table3.getTableNumber(), numberOfPeople);
    }

    @Override
    public String orderHealthyFood(int tableNumber, String healthyFoodName) {
        if (tableRepository.byNumber(tableNumber).getTableNumber() == 0){
            return String.format(OutputMessages.WRONG_TABLE_NUMBER, tableNumber);
        }if (healthFoodRepository.foodByName(healthyFoodName).getName() == null){
            return String.format(OutputMessages.NONE_EXISTENT_FOOD, healthyFoodName);
        }else {
            tableRepository.byNumber(tableNumber).orderHealthy(healthFoodRepository.foodByName(healthyFoodName));
            return String.format(OutputMessages.FOOD_ORDER_SUCCESSFUL, healthyFoodName, tableNumber);
        }
    }

    @Override
    public String orderBeverage(int tableNumber, String name, String brand) {
        if (tableRepository.byNumber(tableNumber).getTableNumber() == 0){
            return String.format(OutputMessages.WRONG_TABLE_NUMBER, tableNumber);
        }if (beverageRepository.beverageByName(name , brand).getName() == null){
            return String.format(OutputMessages.NON_EXISTENT_DRINK, name , brand);
        }else {
            tableRepository.byNumber(tableNumber).orderBeverages(beverageRepository.beverageByName(name, brand));
            return String.format(OutputMessages.BEVERAGE_ORDER_SUCCESSFUL, name, tableNumber);
        }
    }

    @Override
    public String closedBill(int tableNumber) {
        double bill = tableRepository.byNumber(tableNumber).bill();
        total += bill;
        tableRepository.byNumber(tableNumber).clear();
        //TODO:
        return String.format(OutputMessages.BILL, tableNumber, bill);
    }


    @Override
    public String totalMoney() {
        for (Table table: tableRepository.getAllEntities()) {
            total += table.bill();
        }
        //TODO:
        return String.format(OutputMessages.TOTAL_MONEY , total);
    }
}
