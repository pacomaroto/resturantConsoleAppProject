package restaurant.repositories;


import restaurant.entities.tables.BaseTable;
import restaurant.entities.tables.interfaces.Table;
import restaurant.repositories.interfaces.TableRepository;

import java.util.ArrayList;
import java.util.List;

public class TableRepositoryImp implements TableRepository<Table> {
    List<Table> tables = new ArrayList<>();
    @Override
    public List<Table> getAllEntities() {
        return tables;
    }

    @Override
    public void add(Table entity) {
        tables.add(entity);
    }

    @Override
    public Table byNumber(int number) {
        Table curTable = new BaseTable();
        for (Table table:tables){
            if (table.getTableNumber() == number){
                curTable = table;
            }
        }
        return curTable;
    }

}
