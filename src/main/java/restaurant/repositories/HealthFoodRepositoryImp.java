package restaurant.repositories;

import restaurant.entities.healthyFoods.Food;
import restaurant.entities.healthyFoods.interfaces.HealthyFood;
import restaurant.repositories.interfaces.HealthFoodRepository;

import java.util.ArrayList;
import java.util.List;

public class HealthFoodRepositoryImp implements HealthFoodRepository<HealthyFood> {

    List<HealthyFood> healthyFoods = new ArrayList<>();

    @Override
    public HealthyFood foodByName(String name) {
        HealthyFood healthyF = new Food();
        for (HealthyFood healthyFood : healthyFoods) {
//            if (healthyFood.getName() == null){
//                break;
//            }
            if (healthyFood.getName().equals(name)) {
                healthyF = healthyFood;
                break;
            }
        }
        return healthyF ;
    }

    @Override
    public List<HealthyFood> getAllEntities() {
        return healthyFoods;
    }

    @Override
    public void add(HealthyFood entity) {
        healthyFoods.add(entity);
    }
}
