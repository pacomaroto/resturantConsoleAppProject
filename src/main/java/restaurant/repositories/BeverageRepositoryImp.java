package restaurant.repositories;
import restaurant.entities.drinks.BaseBeverage;
import restaurant.entities.drinks.interfaces.Beverages;
import restaurant.repositories.interfaces.BeverageRepository;


import java.util.ArrayList;
import java.util.List;

public class BeverageRepositoryImp implements BeverageRepository<Beverages> {
    List<Beverages> drinks = new ArrayList<>();
    @Override
    public Beverages beverageByName(String drinkName, String drinkBrand) {
        Beverages curDrink = new BaseBeverage();
        for (Beverages drink: drinks) {
            if (drink.getName().equals(drinkName) && drink.getBrand().equals(drinkBrand)){
                curDrink = drink;
            }
        }
        return curDrink;
    }

    @Override
    public List<Beverages> getAllEntities() {
        return drinks;
    }

    @Override
    public void add(Beverages entity) {
        drinks.add(entity);
    }
}
