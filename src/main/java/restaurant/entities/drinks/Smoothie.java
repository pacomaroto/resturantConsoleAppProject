package restaurant.entities.drinks;

import restaurant.entities.drinks.BaseBeverage;

public class Smoothie extends BaseBeverage {
    public Smoothie(String name, int counter,  String brand) {
        super(name, counter, 4.5, brand);
    }
}
