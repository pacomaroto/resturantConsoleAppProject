package restaurant.entities.tables;

import restaurant.common.ExceptionMessages;
import restaurant.entities.drinks.interfaces.Beverages;
import restaurant.entities.healthyFoods.interfaces.HealthyFood;
import restaurant.entities.tables.interfaces.Table;

import java.util.ArrayList;
import java.util.List;

public class BaseTable implements Table {
    List<HealthyFood> healthyFood = new ArrayList<>();
    List<Beverages> beveragesList = new ArrayList<>();
    int tableNumber;
    int size;
    int numberOfPeople;
    double pricePerPerson = 0;
    boolean isTableReserved;
    double bill; //price for all ppl

    public BaseTable(int tableNumber, int size, double pricePerPerson) {
        this.tableNumber = tableNumber;
        setSize(size);
        this.pricePerPerson = pricePerPerson;
    }

    public BaseTable() {
    }

    @Override
    public int getTableNumber() {
        return tableNumber;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int numberOfPeople() {
        return numberOfPeople;
    }

    @Override
    public double pricePerPerson() {
        return pricePerPerson;
    }

    @Override
    public boolean isReservedTable() {
        return isTableReserved;
    }

    @Override
    public double allPeople() {
        return bill;
    }

    @Override
    public void reserve(int numberOfPeople) {
        if (numberOfPeople <= getSize()){
            isTableReserved = true;
            setNumberOfPeople(numberOfPeople);
        }
    }

    @Override
    public void orderHealthy(HealthyFood food) {
        bill += food.getPrice();
        healthyFood.add(food);
    }

    @Override
    public void orderBeverages(Beverages beverages) {
        bill += beverages.getPrice();
        beveragesList.add(beverages);
    }

    @Override
    public double bill() {
        bill = getPricePerPerson() * getNumberOfPeople();
        return bill;
    }

    @Override
    public void clear() {
        beveragesList.clear();
        healthyFood.clear();
        isTableReserved = false;
        setNumberOfPeople(0);
        setBill(0);
    }

    @Override
    public String tableInformation() {
        String info = String.format("Table number: %d, Size: %d, Type %b, All price %.2f",
                tableNumber, size, isTableReserved, bill);
        return info;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public double getPricePerPerson() {
        return pricePerPerson;
    }

    public void setBill(double bill) {
        this.bill = bill;
    }

    public void setSize(int size) {
        if (size < 1){
            throw new IllegalArgumentException(ExceptionMessages.INVALID_TABLE_SIZE);
        }
        this.size = size;
    }
    @Override
    public List<HealthyFood> getHealthyFood() {
        return healthyFood;
    }
    @Override
    public List<Beverages> getBeveragesList() {
        return beveragesList;
    }
}
