package restaurant.entities.tables;

public class InGarden extends BaseTable{

    public InGarden(int tableNumber, int size) {
        super(tableNumber, size, 4.5);
    }
}
