package restaurant.entities.tables;

public class Indoors extends BaseTable {

    public Indoors(int tableNumber, int size) {
        super(tableNumber, size, 3.5);
    }
}
