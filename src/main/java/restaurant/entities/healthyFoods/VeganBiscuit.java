package restaurant.entities.healthyFoods;

public class VeganBiscuit extends Food{

    public VeganBiscuit(String name,  double price) {
        super(name, 205, price);
    }
}
