package restaurant.entities.healthyFoods;

public class Salad extends Food{
    private final double InitialSaladPortion = 150;

    public Salad(String name, double price) {
        super(name, 150, price);
    }

    public Salad(String name, double portion, double price) {
        super(name, portion, price);
    }
}
