package restaurantTest.repositories;


import org.junit.Assert;
import org.junit.Test;
import restaurant.entities.drinks.BaseBeverage;
import restaurant.entities.drinks.interfaces.Beverages;
import restaurant.entities.healthyFoods.Food;
import restaurant.repositories.BeverageRepositoryImp;
import restaurant.repositories.interfaces.BeverageRepository;

import static org.mockito.Mockito.mock;

public class BeverageRepository_Test {
    BeverageRepository<Beverages> beverageRepository = new BeverageRepositoryImp();

    @Test
    public void checkIsTheList_containsOneObject() {
        //Arrange
        BaseBeverage baseBeverage = new BaseBeverage("name", 1 , 1, "brand");
        //Act
        beverageRepository.add(baseBeverage);
        //Assert
        Assert.assertEquals(1, beverageRepository.getAllEntities().size());
    }
    @Test
    public void checkIsTheList_returnObj_byName(){
        //Arrange
        String name = "name";
        String brand = "brand";
        BaseBeverage baseBeverage = new BaseBeverage("name", 1 , 1, "brand");
        //Act
        beverageRepository.add(baseBeverage);
        //Assert
        Assert.assertEquals(baseBeverage, beverageRepository.beverageByName(name, brand));
    }
    @Test
    public void check_Is_Method_return_null(){
        BaseBeverage baseBeverage = new BaseBeverage("namee", 1 , 1, "brand");
        beverageRepository.add(baseBeverage);
        Assert.assertNull(beverageRepository.beverageByName("name", ""));
    }
}
