package restaurantTest.repositories;

import org.junit.Assert;
import org.junit.Test;
import restaurant.entities.healthyFoods.Food;
import restaurant.entities.healthyFoods.interfaces.HealthyFood;
import restaurant.repositories.HealthFoodRepositoryImp;
import restaurant.repositories.interfaces.HealthFoodRepository;

public class HealthFoodRepository_Test {
    HealthFoodRepository<HealthyFood> healthFoodRepository = new HealthFoodRepositoryImp();

    @Test
    public void checkIsTheList_containsOneObject() {
        //Arrange
        Food food = new Food("name", 1, 1);
        //Act
        healthFoodRepository.add(food);
        //Assert
        Assert.assertEquals(1, healthFoodRepository.getAllEntities().size());
    }
    @Test
    public void checkIsTheList_returnObj_byName(){
        //Arrange
        String name = "name";
        Food food = new Food("name", 1, 1);
        //Act
        healthFoodRepository.add(food);
        //Assert
        Assert.assertEquals(food, healthFoodRepository.foodByName(name));
    }
    @Test
    public void check_Is_Method_return_null(){
        Food food = new Food("namee", 1, 1);
        healthFoodRepository.add(food);
        Assert.assertNull(healthFoodRepository.foodByName("name"));
    }

}
