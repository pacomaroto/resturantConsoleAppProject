package restaurantTest.repositories;

import org.junit.Assert;
import org.junit.Test;
import restaurant.entities.tables.BaseTable;
import restaurant.entities.tables.interfaces.Table;
import restaurant.repositories.TableRepositoryImp;
import restaurant.repositories.interfaces.TableRepository;

public class TableRepository_Test {
    TableRepository<Table> tableRepository = new TableRepositoryImp();

    @Test
    public void checkIsTheList_containsOneObject() {
        //Arrange
        Table table = new BaseTable(1, 1, 1);
        //Act
        tableRepository.add(table);
        //Assert
        Assert.assertEquals(1, tableRepository.getAllEntities().size());
    }
    @Test
    public void checkIsTheList_returnObj_number(){
        //Arrange
        int number = 1;
        Table table = new BaseTable(1, 1, 1);
        //Act
        tableRepository.add(table);
        //Assert
        Assert.assertEquals(table, tableRepository.byNumber(number));
    }

}
