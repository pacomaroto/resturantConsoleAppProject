package restaurantTest.models;

import org.junit.Test;
import restaurant.entities.tables.BaseTable;


public class Tables_Test {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_size_is_zero(){
        BaseTable table = new BaseTable(1,0,1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_size_is_negative(){
        BaseTable table = new BaseTable(1,-1,1);
    }
}
