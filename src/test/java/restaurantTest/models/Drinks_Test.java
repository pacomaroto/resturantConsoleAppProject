package restaurantTest.models;

import org.junit.Test;
import restaurant.entities.drinks.BaseBeverage;
import restaurant.entities.drinks.Fresh;

public class Drinks_Test {

@Test(expected = IllegalArgumentException.class)
public void constructor_should_throwErr_when_name_is_null(){
        BaseBeverage drink = new Fresh(null, 1, "brand");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_name_is_with_space(){
        BaseBeverage drink = new Fresh("asdas asdasd", 1, "brand");
    }
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_price_is_zero(){
    BaseBeverage drink = new BaseBeverage("name", 1, 0, "brand" );
    }
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_price_is_Negative(){
        BaseBeverage drink = new BaseBeverage("name", 1, -1, "brand" );
    }
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_brand_is_null(){
        BaseBeverage drink = new Fresh("name", 1, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_brand_is_with_space(){
        BaseBeverage drink = new Fresh("name", 1, "bra nd");
    }
}
