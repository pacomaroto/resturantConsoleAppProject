package restaurantTest.models;

import org.junit.Test;
import restaurant.entities.healthyFoods.Food;

public class Foods_Test {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_price_is_zero(){
       Food food = new Food("name" , 1, 0);
    }
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwErr_when_price_is_negative(){
        Food food = new Food("name" , 1, -1);
    }
}
