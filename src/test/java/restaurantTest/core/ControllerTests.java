package restaurantTest.core;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import restaurant.common.OutputMessages;
import restaurant.common.enums.BeveragesType;
import restaurant.common.enums.HealthyFoodType;
import restaurant.common.enums.TableType;
import restaurant.core.ControllerImpl;
import restaurant.core.interfaces.Controller;
import restaurant.entities.drinks.interfaces.Beverages;
import restaurant.entities.healthyFoods.interfaces.HealthyFood;
import restaurant.entities.tables.BaseTable;
import restaurant.entities.tables.Indoors;
import restaurant.entities.tables.interfaces.Table;
import restaurant.repositories.BeverageRepositoryImp;
import restaurant.repositories.HealthFoodRepositoryImp;
import restaurant.repositories.TableRepositoryImp;
import restaurant.repositories.interfaces.BeverageRepository;
import restaurant.repositories.interfaces.HealthFoodRepository;
import restaurant.repositories.interfaces.TableRepository;


public class ControllerTests {
    public HealthFoodRepository<HealthyFood> healthFoodRepository = new HealthFoodRepositoryImp();
    public BeverageRepository<Beverages> beverageRepository = new BeverageRepositoryImp();
    public TableRepository<Table> tableRepository = new TableRepositoryImp();
    public Controller controller;

    @Before
    public void before(){
        controller = new ControllerImpl(healthFoodRepository, beverageRepository, tableRepository);
    }


    @Test
    public void checkIsMethod_createSalad(){
        //Arrange
        //Act
        controller.addHealthyFood(HealthyFoodType.Salad.toString(), 1, "name");
        //Assert
        Assert.assertEquals(1, healthFoodRepository.getAllEntities().size());
    }
    @Test
    public void checkIsMethod_createVeganBiscuit(){
        //Arrange
        //Act
        controller.addHealthyFood(HealthyFoodType.VeganBiscuits.toString(), 1, "name");
        //Assert
        Assert.assertEquals(1, healthFoodRepository.getAllEntities().size());
    }

    @Test
    public void checkIsMethod_createFresh(){
        //Arrange
        //Act
        controller.addBeverage(BeveragesType.Fresh.toString(), 1 , "Adfs", "Fresh");
        //Assert
        Assert.assertEquals(1, beverageRepository.getAllEntities().size());
    }
    @Test
    public void checkIsMethod_createSmoothie(){
        //Arrange
        //Act
        controller.addBeverage(BeveragesType.Smoothie.toString(), 1 , "Adfs", "Fresh");
        //Assert
        Assert.assertEquals(1, beverageRepository.getAllEntities().size());
    }

    @Test
    public void checkIsMethod_createTableIndoor(){
        //Arrange
        //Act
        controller.addTable(TableType.Indoors.toString(), 1, 1);
        //Assert
        Assert.assertEquals(1, tableRepository.getAllEntities().size());
    }
    @Test
    public void checkIsMethod_createTableInGarden(){
        //Arrange
        //Act
        controller.addTable(TableType.InGarden.toString(), 1, 1);
        //Assert
        Assert.assertEquals(1, tableRepository.getAllEntities().size());
    }

    @Test
    public void checkIsReserveMethod_reserve(){
        //Arrange
        controller.addTable(TableType.InGarden.toString(), 1, 1);
        //Act
        controller.reserve(1);
        //Assert
        Assert.assertTrue(tableRepository.byNumber(1).isReservedTable());
    }
    @Test
    public void checkIsOrderFoodMethod_orderFood(){
        //Arrange
        controller.addTable(TableType.InGarden.toString(), 1, 1);
        controller.reserve(1);
        controller.addHealthyFood(HealthyFoodType.VeganBiscuits.toString(), 1, "someFood");
        //Act
        controller.orderHealthyFood(1, "someFood");
        //Assert
        Assert.assertEquals(1, tableRepository.byNumber(1).getHealthyFood().size());
    }
    @Test
    public void checkIsOrderDrinkMethod_orderDrink(){
        //Arrange
        controller.addTable(TableType.InGarden.toString(), 1, 1);
        controller.reserve(1);
        controller.addBeverage(BeveragesType.Fresh.toString(), 1, "brand", "name");
        //Act
        controller.orderBeverage(1, "name" , "brand");
        //Assert
        Assert.assertEquals(1, tableRepository.byNumber(1).getBeveragesList().size());
    }
    @Test
    public void checkIsMethod_closeBill_clearBill(){
        //Arrange
        BaseTable table = new BaseTable(1, 1,1);
        table.setBill(5);
        //Act
        controller.closedBill(1);
        //Assert
        Assert.assertEquals(0.0, table.bill());
    }
    @Test
    public void checkIsMethod_totalMoney_returnTotalMoney(){
        //Arrange
        BaseTable table = new BaseTable(1, 1,1);
        BaseTable table1 = new BaseTable(2, 1,1);
        table.setBill(5);
        table1.setBill(2.5);
        //Act
        controller.closedBill(1);
        controller.closedBill(2);
        String expectedValue = String.format(OutputMessages.TOTAL_MONEY, table.bill()+table1.bill());
        //Assert
        Assert.assertEquals(expectedValue, controller.totalMoney());
    }
    @Test(expected = IllegalArgumentException.class)
    public void check_isAddDrinkMethod_throwExc_whenDrinkAlreadyExist(){
        controller.addBeverage(BeveragesType.Fresh.toString(), 1, "brand", "name");
        controller.addBeverage(BeveragesType.Smoothie.toString(), 1, "brand", "name");
    }
    @Test(expected = IllegalArgumentException.class)
    public void check_isMethodThrowExc_whenFoodAlreadyExist(){
        //Arrange
        //Act
        //Assert
        controller.addHealthyFood(HealthyFoodType.VeganBiscuits.toString(), 1, "name");
        controller.addHealthyFood(HealthyFoodType.VeganBiscuits.toString(), 1, "name");
    }
    @Test(expected = IllegalArgumentException.class)
    public void check_isAddTableMethod_throwExc_whenTableAlreadyExist(){
        //Arrange
        //Act
        //Assert
        controller.addTable(TableType.Indoors.toString(), 1, 2);
        controller.addTable(TableType.InGarden.toString(),1 , 12);
    }
    @Test(expected = IllegalArgumentException.class)
    public void check_isReservation_throwExc_whenNumberOfPeople_isZero(){
        //Arrange
        //Act
        //Assert
        controller.reserve(0);
    }
    @Test(expected = IllegalArgumentException.class)
    public void check_isReservation_throwExc_whenNumberOfPeople_isNegative(){
        //Arrange
        //Act
        //Assert
        controller.reserve(-1);
    }
    @Test
    public void check_isReservation_returnRightString_whenNumberOfPeople_isHigherThenSeats(){
        //Arrange
        controller.addTable(TableType.InGarden.toString(), 1, 5);
        String expectedString = String.format(OutputMessages.RESERVATION_NOT_POSSIBLE, 7);
        //Act
        //Assert
        Assert.assertEquals(expectedString, controller.reserve(7));
    }
    @Test
    public void check_isOrderFood_returnRightString_whenTableNumber_isWrong(){
        //Arrange
        String expectedString = String.format(OutputMessages.WRONG_TABLE_NUMBER, 1);
        //Act
        controller.addHealthyFood(HealthyFoodType.VeganBiscuits.toString(), 1, "name");
        //Assert
        Assert.assertEquals(expectedString, controller.orderHealthyFood(1, "name"));
    }
    @Test
    public void check_isOrderFood_returnRightString_whenFoodName_isWrong(){
        //Arrange
        String expectedString = String.format(OutputMessages.NONE_EXISTENT_FOOD, "name");
        //Act
        controller.addTable(TableType.InGarden.toString(), 1, 5);
        //Assert
        Assert.assertEquals(expectedString, controller.orderHealthyFood(1, "name"));
    }
    @Test
    public void check_isOrderDrink_returnRightString_whenTableNumber_isWrong(){
        //Arrange
        String expectedString = String.format(OutputMessages.WRONG_TABLE_NUMBER, 1);
        //Act
        controller.addBeverage(BeveragesType.Fresh.toString(), 1, "brand", "name");
        //Assert
        Assert.assertEquals(expectedString, controller.orderBeverage(1, "name", "brand"));
    }
    @Test
    public void check_isOrderDrink_returnRightString_whenDrinkName_isWrong(){
        //Arrange
        String expectedString = String.format(OutputMessages.NON_EXISTENT_DRINK, "name", "brand");
        //Act
        controller.addTable(TableType.InGarden.toString(), 1, 5);
        controller.addBeverage(BeveragesType.Fresh.toString(), 1, "brand", "wrongName");
        //Assert
        Assert.assertEquals(expectedString, controller.orderBeverage(1, "name", "brand"));
    }

}
